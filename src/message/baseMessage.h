#ifndef BASEMESSAGE_H
#define BASEMESSAGE_H

#include <QByteArray>

#include <cstdint>

class BaseMessage {
public:
    static const int HEADER_SIZE = 4;
    static const QByteArray START_MARKER_ARRAY;
    const static uint8_t END_BYTE = 0xAA;

    BaseMessage() = default;
};

#endif // BASEMESSAGE_H
