#include "message/alertMessage.h"

#include <QByteArray>
#include <QString>

AlertMessage::AlertMessage(const QByteArray& data)
{
    const uint8_t* ptr = (uint8_t*)(data.data());

    alarmCode = static_cast<AlarmCode>(*(ptr + 4));
    detail1 = *(ptr + 5);
    // detail2 = *(ptr + 6);
    // crc = *((uint16_t*)(ptr + 7));
}

QString to_string(AlertMessage::AlarmCode code)
{
    switch (code) {
    case AlertMessage::AlarmCode::NO_ALARM:
        return "NO_ALARM";
    case AlertMessage::AlarmCode::POWER_SUPPLY:
        return "POWER_SUPPLY";
    case AlertMessage::AlarmCode::BATTERY_SUPPLY:
        return "BATTERY_SUPPLY";
    case AlertMessage::AlarmCode::OXYGEN:
        return "OXYGEN";
    case AlertMessage::AlarmCode::AIR_PRESSURE:
        return "AIR_PRESSURE";
    case AlertMessage::AlarmCode::AIR_VOLUME:
        return "AIR_VOLUME";
    case AlertMessage::AlarmCode::HOSES_DISCONNECTED:
        return "HOSES_DISCONNECTED";
    case AlertMessage::AlarmCode::BREATHS_PER_MINUTE:
        return "BREATHS_PER_MINUTE";
    case AlertMessage::AlarmCode::DEVICE:
        return "DEVICE";
    }
    return "UNKNOWN";
}
