#ifndef ADDITIONALSETTINGSMESSAGE_H
#define ADDITIONALSETTINGSMESSAGE_H

#include "baseMessage.h"

#include <QByteArray>

class AdditionalSettingsMessage : public BaseMessage {
public:
    static const uint8_t MSG_ID = 101;
    static const uint8_t SIZE = 44;

    AdditionalSettingsMessage(float P, float I, float D, float max, float min,
        float deltaP, float deltaT, float deltaF);

    QByteArray getData() const;

private:
    float P, I, D, max, min, deltaP, deltaT, deltaF;
};

#endif // ADDITIONALSETTINGSMESSAGE_H
