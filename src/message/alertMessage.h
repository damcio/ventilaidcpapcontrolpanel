#ifndef ALERTMESSAGE_H
#define ALERTMESSAGE_H

#include "baseMessage.h"

class QByteArray;

class AlertMessage : BaseMessage {
public:
    constexpr static uint8_t MSG_ID = 10;

    enum class AlarmCode : uint8_t {
        NO_ALARM = 0,
        POWER_SUPPLY,
        BATTERY_SUPPLY,
        OXYGEN,
        AIR_PRESSURE,
        AIR_VOLUME,
        HOSES_DISCONNECTED,
        BREATHS_PER_MINUTE,
        DEVICE
    };

    AlertMessage(const QByteArray& data);

    AlarmCode getAlarmCode() { return alarmCode; }
    uint8_t getDetail1() { return detail1; }

private:
    AlarmCode alarmCode;
    uint8_t detail1;
    // uint8_t detail2; unused
    // uint16_t crc; unused
};

QString to_string(AlertMessage::AlarmCode code);

#endif // ALERTMESSAGE_H
