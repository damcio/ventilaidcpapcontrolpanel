#ifndef DEBUGMESSAGE_H
#define DEBUGMESSAGE_H

#include "baseMessage.h"

class QByteArray;

class DebugMessage : public BaseMessage {
public:
    static const uint8_t MSG_ID = 0x50;

    DebugMessage(const QByteArray& data);

    float getValue(int idx) const;
    uint8_t getIntergerValue() const;

private:
    float debugValue[4];
    uint8_t intergerValue;
};

#endif // DEBUGMESSAGE_H
