#ifndef ERRORMESSAGE_H
#define ERRORMESSAGE_H

#include "baseMessage.h"

class QByteArray;

class ErrorMessage : public BaseMessage {
public:
    static const uint8_t MSG_ID = 99;

    ErrorMessage(const QByteArray& data);

    uint8_t getErrorId() const;

private:
    uint8_t errorId;
};

#endif // ERRROMESSAGE_H
