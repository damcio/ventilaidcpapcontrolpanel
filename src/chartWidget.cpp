#include "chartWidget.h"
#include "ui_chartWidget.h"

#include <limits>

ChartWidget::ChartWidget(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::chartWidget)
    , chart(new QChart)
    , dataSeries(new QLineSeries(this))
    , axisX(new QValueAxis(this))
    , axisY(new QValueAxis(this))
    , xPosition(0)
    , unitTemplate("%1")
{
    ui->setupUi(this);

    ui->chartView->setChart(chart);
    ui->chartView->setRenderHint(QPainter::Antialiasing);

    chart->addSeries(dataSeries);
    chart->addAxis(axisX, Qt::AlignBottom);
    chart->addAxis(axisY, Qt::AlignLeft);

    dataSeries->attachAxis(axisX);
    dataSeries->attachAxis(axisY);

    axisX->setTickCount(5);
    axisX->setRange(xPosition - 20, xPosition);
    axisX->setLabelsVisible(false);
}

ChartWidget::~ChartWidget()
{
    delete ui;
}

void ChartWidget::setup(QString settingsKey, QString unitTemplate)
{
    this->settingsKey = settingsKey;
    this->unitTemplate = unitTemplate;

    QSettings settings;
    settings.beginGroup(settingsKey);
    axisY->setRange(settings.value("min").toFloat(), settings.value("max").toFloat());

    // TODO: move to construtor after removing debug dialogs?
    chart->setMargins(QMargins(0, 0, 0, 0));
    chart->setContentsMargins(0, 0, 0, 0);
    chart->layout()->setContentsMargins(0, 0, 0, 0);
    chart->legend()->hide();
    axisY->setTickCount(2);
    ui->bottomBar->setVisible(false);
}

void ChartWidget::setupDebugChart(QString name, QString displayName)
{
    this->settingsKey = name;
    this->displayName = displayName;

    dataSeries->setName(displayName);

    QSettings settings;
    settings.beginGroup(name);

    int chartWindowSeconds = settings.value("chartWindowSeconds").toInt();

    axisX->setTitleText(QString("Showing last %1 seconds").arg(chartWindowSeconds));

    ui->showLast->setRange(0, 600);
    ui->showLast->setValue(chartWindowSeconds);
    ui->axisYMin->setRange(std::numeric_limits<double>::lowest(), std::numeric_limits<double>::max());
    ui->axisYMax->setRange(std::numeric_limits<double>::lowest(), std::numeric_limits<double>::max());
    ui->axisYMin->setValue(settings.value("axisYMin").toInt());
    ui->axisYMax->setValue(settings.value("axisYMax").toInt());

    axisY->setRange(settings.value("axisYMin").toInt(), settings.value("axisYMax").toInt());

    connect(ui->axisYMin, static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [this](int value) {
        QSettings settings;
        settings.beginGroup(this->settingsKey);
        settings.setValue("axisYMin", value);
        settings.endGroup();
        axisY->setMin(value);
    });
    connect(ui->axisYMax, static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [this](int value) {
        QSettings settings;
        settings.beginGroup(this->settingsKey);
        settings.setValue("axisYMax", value);
        settings.endGroup();
        axisY->setMax(value);
    });
    connect(ui->showLast, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), [this](int value) {
        QSettings settings;
        settings.beginGroup(this->settingsKey);
        settings.setValue("chartWindowSeconds", value);
        settings.endGroup();
        axisX->setTitleText(QString("Showing last %1 seconds").arg(value));
    });

    settings.endGroup();
}

void ChartWidget::addValue(float value)
{
    dataSeries->append(xPosition, value);

    QSettings settings;
    int refreshRate = settings.value("refreshRateHz").toInt();
    settings.beginGroup(settingsKey);
    int ticks = settings.value("chartWindowSeconds").toInt() * refreshRate;

    qreal stepValue = (axisX->max() - axisX->min()) / ticks;

    axisX->setRange(
        axisX->min() + stepValue,
        axisX->max() + stepValue);
    xPosition += stepValue;

    settings.endGroup();

    // remove invisible data
    if (dataSeries->count() > 2 * ticks)
        dataSeries->removePoints(0, ticks);

    ui->value->setText(unitTemplate.arg(value, 4, 'f', 1));
}

void ChartWidget::clear()
{
    dataSeries->clear();
    xPosition = 0;
    axisX->setRange(xPosition - 20, xPosition);

    ui->value->setText(unitTemplate.arg("?.?"));
}
