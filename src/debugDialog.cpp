#include "debugDialog.h"
#include "ui_debugDialog.h"

DebugDialog::DebugDialog(QWidget* parent)
    : QDialog(parent)
    , ui(new Ui::DebugDialog)
{
    ui->setupUi(this);

    ui->chart1->setupDebugChart("debugChart1", "Debug 1");
    ui->chart2->setupDebugChart("debugChart2", "Debug 2");
    ui->chart3->setupDebugChart("debugChart3", "Debug 3");
    ui->chart4->setupDebugChart("debugChart4", "Debug 4");
}

DebugDialog::~DebugDialog()
{
    delete ui;
}

void DebugDialog::clear()
{
    ui->chart1->clear();
    ui->chart2->clear();
    ui->chart3->clear();
    ui->chart4->clear();
}

void DebugDialog::onNewDebugMessage(DebugMessage msg)
{
    ui->chart1->addValue(msg.getValue(0));
    ui->chart2->addValue(msg.getValue(1));
    ui->chart3->addValue(msg.getValue(2));
    ui->chart4->addValue(msg.getValue(3));
}
