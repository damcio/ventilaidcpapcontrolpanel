#ifndef SERIALOVERSEER_H
#define SERIALOVERSEER_H

#include <QObject>

#include "messageHandler.h"
#include "serial.h"
#include "utils.h"

class SerialOverseer : public QObject {
    Q_OBJECT
public:
    enum class State {
        Normal,
        Reconnecting
    };

    explicit SerialOverseer(QObject* parent, Utils::Target target);

    bool isConnected();
    void disconnect();
    bool isReconnecting();
    void stopReconnecting();

public slots:
    void sendMessage(StatusMessage msg);
    void sendMessage(AdditionalSettingsMessage msg);
    void open(QString portName, QSerialPort::BaudRate baudRate);

signals:
    void connected(QString portName);
    void disconnected(Serial::DisconnectionReason reason, QString msg);
    void reconnectingStarted(QString message);
    void newStatusMessage(StatusMessage msg);
    void newDebugMessage(DebugMessage msg);
    void statusMessageRequest();

private slots:
    void openPreviousSerial();

private:
    Serial* serial;
    QTimer* sendTimer;
    QTimer* reconnectTimer;
    State state;
    MessageHandler* messageHandler;
};

#endif // SERIALOVERSEER_H
