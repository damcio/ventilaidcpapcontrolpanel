#ifndef FILELOGGER_H
#define FILELOGGER_H

#include <QObject>

class QFile;

class FileLogger : public QObject {
    Q_OBJECT
public:
    explicit FileLogger(QObject* parent, QString filePath, QString header);

public slots:
    void onNewMessage(QString msg);

private:
    QFile* file;
};

#endif // FILELOGGER_H
