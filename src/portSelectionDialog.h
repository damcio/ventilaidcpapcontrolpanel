#ifndef PORTSELECTIONDIALOG_H
#define PORTSELECTIONDIALOG_H

#include <QDialog>
#include <QSerialPort>

namespace Ui {
class PortSelectionDialog;
}

class PortSelectionDialog : public QDialog {
    Q_OBJECT

public:
    explicit PortSelectionDialog(QWidget* parent = nullptr);
    ~PortSelectionDialog();

    QSerialPort::BaudRate getBaudRate() const;
    QString getSeriaPortName() const;

private:
    Ui::PortSelectionDialog* ui;
};

#endif // PORTSELECTIONDIALOG_H
