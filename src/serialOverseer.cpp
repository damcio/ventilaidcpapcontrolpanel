#include "serialOverseer.h"

SerialOverseer::SerialOverseer(QObject* parent, Utils::Target target)
    : QObject(parent)
    , serial(new Serial(this))
    , sendTimer(new QTimer(this))
    , reconnectTimer(new QTimer(this))
    , state(State::Normal)
    , messageHandler(nullptr)
{
    connect(serial, &Serial::connected, [this, target](QString portName) {
        qDebug() << "Connected to:" << portName;

        emit connected(portName);

        if (isReconnecting())
            stopReconnecting();

        QSettings settings;
        if (settings.value("gatherLogs").toBool() && (target != Utils::Target::Raspberry)) {
            messageHandler = new MessageHandler(this);
            connect(serial, &Serial::newStatusMessage, messageHandler, &MessageHandler::newStatusMessage);
            connect(serial, &Serial::newDebugMessage, messageHandler, &MessageHandler::newDebugMessage);
            connect(serial, &Serial::newAlertMessage, messageHandler, &MessageHandler::newAlertMessage);
            connect(serial, &Serial::newErrorMessage, messageHandler, &MessageHandler::newErrorMessage);
        }

        sendTimer->start(settings.value("sendIntervalMs").toInt());
    });

    connect(serial, &Serial::disconnected, [this](Serial::DisconnectionReason reason, QString msg) {
        emit disconnected(reason, msg);

        if (messageHandler) {
            delete messageHandler;
            messageHandler = nullptr;
        }

        sendTimer->stop();

        // Report errors
        if (reason == Serial::DisconnectionReason::Error)
            qDebug() << "Serial port error: " << msg;

        if (QSettings().value("autoReconnect").toBool() && reason == Serial::DisconnectionReason::Error && state == State::Normal) {
            state = State::Reconnecting;
            reconnectTimer->start(1000);
            emit reconnectingStarted(msg);
        }
    });

    connect(reconnectTimer, &QTimer::timeout, this, &SerialOverseer::openPreviousSerial);
    connect(sendTimer, &QTimer::timeout, this, &SerialOverseer::statusMessageRequest);
    connect(serial, &Serial::newStatusMessage, this, &SerialOverseer::newStatusMessage);
    connect(serial, &Serial::newDebugMessage, this, &SerialOverseer::newDebugMessage);
}

bool SerialOverseer::isConnected()
{
    return serial->isConnected();
}

void SerialOverseer::disconnect()
{
    serial->disconnect();
}

bool SerialOverseer::isReconnecting()
{
    return state == State::Reconnecting;
}

void SerialOverseer::stopReconnecting()
{
    reconnectTimer->stop();
    state = State::Normal;
}

void SerialOverseer::sendMessage(StatusMessage msg)
{
    serial->sendMessage(msg);
}

void SerialOverseer::sendMessage(AdditionalSettingsMessage msg)
{
    serial->sendMessage(msg);
}

void SerialOverseer::open(QString portName, QSerialPort::BaudRate baudRate)
{
    serial->open(portName, baudRate);
}

void SerialOverseer::openPreviousSerial()
{
    QSettings settings;

    serial->open(
        settings.value("lastSelectedPort").toString(),
        static_cast<QSerialPort::BaudRate>(settings.value("lastSelectedBaudRate").toInt()));
}
