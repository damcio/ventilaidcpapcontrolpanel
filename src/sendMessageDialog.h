#ifndef SENDMESSAGEDIALOG_H
#define SENDMESSAGEDIALOG_H

#include <QDialog>

#include "message/additionalSettingsMessage.h"

namespace Ui {
class SendMessageDialog;
}

class SendMessageDialog : public QDialog {
    Q_OBJECT

public:
    explicit SendMessageDialog(QWidget* parent = nullptr);
    ~SendMessageDialog();

    AdditionalSettingsMessage getMessage() const;

private:
    Ui::SendMessageDialog* ui;
};

#endif // SENDMESSAGEDIALOG_H
