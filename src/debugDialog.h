#ifndef DEBUGDIALOG_H
#define DEBUGDIALOG_H

#include "message/debugMessage.h"

#include <QDialog>

namespace Ui {
class DebugDialog;
}

class DebugDialog : public QDialog {
    Q_OBJECT

public:
    explicit DebugDialog(QWidget* parent = nullptr);
    ~DebugDialog();

    void clear();

public slots:
    void onNewDebugMessage(DebugMessage msg);

private:
    Ui::DebugDialog* ui;
};

#endif // DEBUGDIALOG_H
