#include "portSelectionDialog.h"
#include "ui_portSelectionDialog.h"

#include <QSerialPortInfo>
#include <QSettings>

PortSelectionDialog::PortSelectionDialog(QWidget* parent)
    : QDialog(parent)
    , ui(new Ui::PortSelectionDialog)
{
    ui->setupUi(this);

    const auto availablePorts = QSerialPortInfo::availablePorts();

    for (const auto& port : availablePorts)
        ui->portComboBox->addItem(port.portName());

    int idx = ui->portComboBox->findText(QSettings().value("lastSelectedPort").toString());
    if (idx != -1)
        ui->portComboBox->setCurrentIndex(idx);

    ui->baudRatecomboBox->addItem(QString::number(QSerialPort::Baud115200), QSerialPort::Baud115200);
    ui->baudRatecomboBox->addItem(QString::number(QSerialPort::Baud57600), QSerialPort::Baud57600);
    ui->baudRatecomboBox->addItem(QString::number(QSerialPort::Baud38400), QSerialPort::Baud38400);
    ui->baudRatecomboBox->addItem(QString::number(QSerialPort::Baud19200), QSerialPort::Baud19200);
    ui->baudRatecomboBox->addItem(QString::number(QSerialPort::Baud9600), QSerialPort::Baud9600);
    ui->baudRatecomboBox->addItem(QString::number(QSerialPort::Baud4800), QSerialPort::Baud4800);
    ui->baudRatecomboBox->addItem(QString::number(QSerialPort::Baud2400), QSerialPort::Baud2400);
    ui->baudRatecomboBox->addItem(QString::number(QSerialPort::Baud1200), QSerialPort::Baud1200);

    idx = ui->baudRatecomboBox->findText(QString::number(QSettings().value("lastSelectedBaudRate").toInt()));
    if (idx != -1)
        ui->baudRatecomboBox->setCurrentIndex(idx);

    connect(this, &QDialog::accepted, [this]() {
        QSettings settings;
        settings.setValue("lastSelectedPort", ui->portComboBox->currentText());
        settings.setValue("lastSelectedBaudRate", ui->baudRatecomboBox->currentData().toInt());
    });
}

PortSelectionDialog::~PortSelectionDialog()
{
    delete ui;
}

QSerialPort::BaudRate PortSelectionDialog::getBaudRate() const
{
    uint baudRate = ui->baudRatecomboBox->currentData().toUInt();
    return static_cast<QSerialPort::BaudRate>(baudRate);
}

QString PortSelectionDialog::getSeriaPortName() const
{
    return ui->portComboBox->currentText();
}
