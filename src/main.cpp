#include "mainWindow.h"
#include "serialOverseer.h"
#include "utils.h"

#include <QApplication>
#include <QDebug>
#include <QSysInfo>

int main(int argc, char* argv[])
{
    QApplication a(argc, argv);
    a.setWindowIcon(QIcon(":/resource/icon_1024x1024.png"));

    Utils::initSettingsWithDefult();
    QString architecture = QSysInfo::currentCpuArchitecture();

    qDebug() << "Current CPU architecute:" << architecture;

    Utils::Target target;
    if (architecture == "arm" || architecture == "arm64")
        target = Utils::Target::Raspberry;
    else
        target = Utils::Target::Desktop;

    SerialOverseer serialOverseer(nullptr, target);

    MainWindow w(nullptr, target, &serialOverseer);
    w.show();

    return a.exec();
}
