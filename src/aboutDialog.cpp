#include "aboutDialog.h"
#include "ui_aboutDialog.h"

AboutDialog::AboutDialog(QWidget* parent)
    : QDialog(parent)
    , ui(new Ui::AboutDialog)
{
    ui->setupUi(this);

    ui->version->setText(QString("Version: %1").arg(GIT_VERSION));
}

AboutDialog::~AboutDialog()
{
    delete ui;
}
