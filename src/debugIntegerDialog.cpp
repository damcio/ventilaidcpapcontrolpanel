#include "debugIntegerDialog.h"
#include "ui_debugIntegerDialog.h"

DebugIntegerDialog::DebugIntegerDialog(QWidget* parent)
    : QDialog(parent)
    , ui(new Ui::DebugIntegerDialog)
{
    ui->setupUi(this);

    ui->chart->setupDebugChart("debugIntegerChart1", "Integer 1");
}

DebugIntegerDialog::~DebugIntegerDialog()
{
    delete ui;
}

void DebugIntegerDialog::clear()
{
    ui->chart->clear();
}

void DebugIntegerDialog::onNewDebugMessage(DebugMessage msg)
{
    ui->chart->addValue(msg.getIntergerValue());
}
