#ifndef CHARTWIDGET_H
#define CHARTWIDGET_H

#include <QWidget>
#include <QtCharts/QLineSeries>
#include <QtCharts/QValueAxis>

namespace Ui {
class chartWidget;
}

using namespace QtCharts;

class ChartWidget : public QWidget {
    Q_OBJECT

public:
    explicit ChartWidget(QWidget* parent = nullptr);
    ~ChartWidget();

    void setup(QString settingsKey, QString unitTemplate);
    void setupDebugChart(QString settingsKey, QString displayName);
    void addValue(float value);
    void clear();

private:
    Ui::chartWidget* ui;

    QChart* chart;
    QLineSeries* dataSeries;
    QValueAxis* axisX;
    QValueAxis* axisY;
    qreal xPosition;

    QString settingsKey;
    QString unitTemplate;
    QString displayName;
};

#endif // CHARTWIDGET_H
