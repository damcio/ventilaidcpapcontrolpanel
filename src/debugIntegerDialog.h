#ifndef DEBUGINTEGERDIALOG_H
#define DEBUGINTEGERDIALOG_H

#include "message/debugMessage.h"

#include <QDialog>

namespace Ui {
class DebugIntegerDialog;
}

class DebugIntegerDialog : public QDialog {
    Q_OBJECT

public:
    explicit DebugIntegerDialog(QWidget* parent = nullptr);
    ~DebugIntegerDialog();

    void clear();

public slots:
    void onNewDebugMessage(DebugMessage msg);

private:
    Ui::DebugIntegerDialog* ui;
};

#endif // DEBUGINTEGERDIALOG_H
