/*
  SerialPassthrough sketch

  Some boards, like the Arduino 101, the MKR1000, Zero, or the Micro, have one
  hardware serial port attached to Digital pins 0-1, and a separate USB serial
  port attached to the IDE Serial Monitor. This means that the "serial
  passthrough" which is possible with the Arduino UNO (commonly used to interact
  with devices/shields that require configuration via serial AT commands) will
  not work by default.

  This sketch allows you to emulate the serial passthrough behaviour. Any text
  you type in the IDE Serial monitor will be written out to the serial port on
  Digital pins 0 and 1, and vice-versa.

  On the 101, MKR1000, Zero, and Micro, "Serial" refers to the USB Serial port
  attached to the Serial Monitor, and "Serial1" refers to the hardware serial
  port attached to pins 0 and 1. This sketch will emulate Serial passthrough
  using those two Serial ports on the boards mentioned above, but you can change
  these names to connect any two serial ports on a board that has multiple ports.

  created 23 May 2016
  by Erik Nyquist
*/

char buf[32] = {
  0xF0, 0x0F,
  1,  // msg_id
  32, // bytes_no
  0, 0, 0, 0, // high pressure
  0, 0, 0, 0, // low pressure
  0, 0, 0, 0, // breaths
  0, 0, 0, 0, // breath proportion
  0, 0, 0, 0, // air volume
  0, 0, 0, 0, 0,
  0, 0, // crc
  0xAA  
};

char debugMsg[24] = {
  0xF0, 0x0F,
  0x50,  // msg_id
  24, // bytes_no
  0, 0, 0, 0, // float1
  0, 0, 0, 0, // float2
  0, 0, 0, 0, // float3
  0, 0, 0, 0, // float4
  0, // forgot
  0, 0, // crc
  0xAA
};

void setup() {
  float* highPressure = (float*)(buf+4);
  *highPressure = 5;

  float* lowPressure = (float*)(buf+8);
  *lowPressure = 4;

  float* breaths = (float*)(buf+12);
  *breaths = 10;

  float* breathsProportion = (float*)(buf+16);
  *breathsProportion = 3;

  float* airVolume = (float*)(buf+20);
  *airVolume = 300;

  Serial.begin(9600);
}

void loop() {
  delay(100);
  Serial.write(buf, 32);

  float* highPressure = (float*)(buf+4);
  *highPressure += 3;
  if (*highPressure > 16)
    *highPressure = 5;

  float* lowPressure = (float*)(buf+8);
  *lowPressure += 1;
  if (*lowPressure > 16)
    *lowPressure = 3;

  float* breaths = (float*)(buf+12);
  *breaths += 3;
  if (*breaths > 40)
    *breaths = 10;

  float* breathsProportion = (float*)(buf+16);
  *breathsProportion += 1;
  if (*breathsProportion > 4)
    *breathsProportion = 2;

  float* airVolume = (float*)(buf+20);
  *airVolume += 3;
  if (600 < *airVolume)
    *airVolume = 300;

  delay(10);

  Serial.write(debugMsg, 25);

  float* float1 = (float*)(debugMsg+4);
  *float1 += 2;
  if (20 < *float1)
    *float1 = 3;

  float* float3 = (float*)(debugMsg+12);
  *float3 += 1;
  if (30 < *float3)
    *float3 = 0;

  uint8_t* debugInt = (uint8_t*)(debugMsg+20);
  *debugInt += 3;
  *debugInt %= 41;
  if (*debugInt < 10)
    *debugInt = 10;

}
