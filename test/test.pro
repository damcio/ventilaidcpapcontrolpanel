QT = core testlib serialport

TARGET = test_ventilaid
CONFIG += qt warn_on depend_includepath testcase

TEMPLATE = app

INCDIR = ../src
INCLUDEPATH += $$INCDIR

HEADERS += \
    $$INCDIR/serial.h \
    $$INCDIR/message/additionalSettingsMessage.h \
    $$INCDIR/message/alertMessage.h \
    $$INCDIR/message/baseMessage.h \
    $$INCDIR/message/debugMessage.h \
    $$INCDIR/message/errorMessage.h \
    $$INCDIR/message/statusMessage.h

SOURCES += \
    $$INCDIR/serial.cpp \
    $$INCDIR/message/additionalSettingsMessage.cpp \
    $$INCDIR/message/alertMessage.cpp \
    $$INCDIR/message/baseMessage.cpp \
    $$INCDIR/message/debugMessage.cpp \
    $$INCDIR/message/errorMessage.cpp \
    $$INCDIR/message/statusMessage.cpp


SOURCES += test_serial.cpp

