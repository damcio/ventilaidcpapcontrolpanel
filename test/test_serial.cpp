#include <QtTest>

#include "serial.h"

class SerialTest : public QObject {
    Q_OBJECT

public:
    SerialTest() = default;

private slots:
    void testCanCreateSerial();
};

void SerialTest::testCanCreateSerial()
{
    Serial serial(nullptr);
    QCOMPARE(serial.isConnected(), false);
}

QTEST_MAIN(SerialTest)
#include "test_serial.moc"
