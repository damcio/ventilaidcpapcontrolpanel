#!/bin/bash

# This script sends VentilAid StatusMessage with fake data to the specified virtual serial port.
# It is useful when you don't have a VentilAid device and want to test only the Control Panel.
#
# Before running this script, one must open a fake connection using socat:
#   socat -d -d pty,raw,echo=0 pty,raw,echo=0
# This will display two pseudo-terminals (for example: /dev/pts/1 and /dev/pts/2).
# The Control Panel application must listen to one of them
# and the second one should be passed as an argument to this script.
#
# To make Control Panel listen to the pseudo-terminal (e.g. /dev/pts/2), change:
#     if (QSerialPortInfo::availablePorts().empty()) {
# to:
#     if (QSerialPortInfo::availablePorts().empty() && !QDir().exists("/dev/pts/2")) {
# in MainWindow::openUserSelectedSerial(). And add:
#     ui->portComboBox->addItem("/dev/pts/2");
# in PortSelectionDialog constructor.
#
# Then you can run the script for example like this:
# ./fakeDataSender.sh /dev/pts/1
#
# The Control Panel application should then be able to connect to /dev/pts/2 and receive our fake data.
# To check if the Control Panel application sends data to the "device", one can simply use:
# cat /dev/pts/1

if [[ -z "$1" ]]; then
  echo "You need to specify a pseudo-terminal to which the data will be sent. E.g.:"
  echo "  $0 /dev/pts/1"
  echo "Available pseudo-terminals:"
  ls /dev/pts/*
  exit 1
fi

target=$1         # pseudo-terminal connected with the one to which Control Panel should listen to
interval=0.1      # how long before sending next message (in seconds)
iterations=10000  # how many messages will be sent

echo "I will send fake VentiAid data to $1 every $interval second(s) $iterations times."

# Values in the arrays below will be sent repeatedly.
hi_press_vals=(5 6 7 8 9 10 11 12 13 14 15 14 13 12 11 10 9 8 7 6)
hi_press_n=${#hi_press_vals[@]}
lo_press_vals=(5 6 7 8 9 10 11 12 13 14 15 14 13 12 11 10 9 8 7 6)
lo_press_n=${#lo_press_vals[@]}
breaths_vals=(10 14 18 22 26 30 34 37 40 37 34 30 26 22 18 14)
breaths_n=${#breaths_vals[@]}
br_prop_vals=(2 3 4 3)
br_prop_n=${#br_prop_vals[@]}
air_vol_vals=(300 350 400 450 500 550 600 550 500 450 400 350)
air_vol_n=${#air_vol_vals[@]}

# The floats array holds 4 bytes for every float. E.g. The bytes for 4.0f are in floats[4].
# As you can see the list of available conversions is very limited.
# TODO: There must be a better way to do this.
floats=()
floats[0]="\x00\x00\x00\x00"
floats[1]="\x00\x00\x80\x3f"
floats[2]="\x00\x00\x00\x40"
floats[3]="\x00\x00\x40\x40"
floats[4]="\x00\x00\x80\x40"
floats[5]="\x00\x00\xa0\x40"
floats[6]="\x00\x00\xc0\x40"
floats[7]="\x00\x00\xe0\x40"
floats[8]="\x00\x00\x00\x41"
floats[9]="\x00\x00\x10\x41"
floats[10]="\x00\x00\x20\x41"

floats[11]="\x00\x00\x30\x41"
floats[12]="\x00\x00\x40\x41"
floats[13]="\x00\x00\x50\x41"
floats[14]="\x00\x00\x60\x41"
floats[15]="\x00\x00\x70\x41"
floats[16]="\x00\x00\x80\x41"
floats[17]="\x00\x00\x88\x41"
floats[18]="\x00\x00\x90\x41"
floats[19]="\x00\x00\x98\x41"
floats[20]="\x00\x00\xa0\x41"

floats[21]="\x00\x00\xa8\x41"
floats[22]="\x00\x00\xb0\x41"
floats[23]="\x00\x00\xb8\x41"
floats[24]="\x00\x00\xc0\x41"
floats[25]="\x00\x00\xc8\x41"
floats[26]="\x00\x00\xd0\x41"
floats[27]="\x00\x00\xd8\x41"
floats[28]="\x00\x00\xe0\x41"
floats[29]="\x00\x00\xe8\x41"
floats[30]="\x00\x00\xf0\x41"

floats[31]="\x00\x00\xf8\x41"
floats[32]="\x00\x00\x00\x42"
floats[33]="\x00\x00\x04\x42"
floats[34]="\x00\x00\x08\x42"
floats[35]="\x00\x00\x0c\x42"
floats[36]="\x00\x00\x10\x42"
floats[37]="\x00\x00\x14\x42"
floats[38]="\x00\x00\x18\x42"
floats[39]="\x00\x00\x1c\x42"
floats[40]="\x00\x00\x20\x42"

floats[300]="\x00\x00\x96\x43"
floats[350]="\x00\x00\xaf\x43"
floats[400]="\x00\x00\xc8\x43"
floats[450]="\x00\x00\xe1\x43"
floats[500]="\x00\x00\xfa\x43"
floats[550]="\x00\x80\x09\x44"
floats[600]="\x00\x00\x16\x44"

hi_press_idx=0
lo_press_idx=0
breaths_idx=0
br_prop_idx=0
air_vol_idx=0
iteration=0
while [[ iteration -lt iterations ]]; do
  hi_press_bytes=${floats[${hi_press_vals[$hi_press_idx]}]}
  lo_press_bytes=${floats[${lo_press_vals[$lo_press_idx]}]}
  breaths_bytes=${floats[${breaths_vals[$breaths_idx]}]}
  br_prop_bytes=${floats[${br_prop_vals[$br_prop_idx]}]}
  air_vol_bytes=${floats[${air_vol_vals[$air_vol_idx]}]}

  echo -n -e "\xf0\x0f\x01\x20${hi_press_bytes}${lo_press_bytes}${breaths_bytes}${br_prop_bytes}${air_vol_bytes}\x00\x00\x00\x00\x00\x00\x00\xaa" > ${target}

  sleep $interval

  iteration=$(( iteration + 1 ))
  hi_press_idx=$(( (hi_press_idx + 1) % hi_press_n ))
  lo_press_idx=$(( (lo_press_idx + 1) % lo_press_n ))
  breaths_idx=$(( (breaths_idx + 1) % breaths_n ))
  br_prop_idx=$(( (br_prop_idx + 1) % br_prop_n ))
  air_vol_idx=$(( (air_vol_idx + 1) % air_vol_n ))
done

